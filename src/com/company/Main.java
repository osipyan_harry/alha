package com.company;

import java.io.*;
import java.util.StringTokenizer;

public class Main {

    public static void main(String[] args) {
        //******************************************************************************************
        // Задание 2
        try {

            PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter("C://222.txt")));
            for (int i = 0; i < 21; i++) {
                writer.print((int) (Math.random() * 21));
                writer.print(',');
            }
            writer.close();

            BufferedReader reader = new BufferedReader(new FileReader("C://222.txt"));
            StringTokenizer st = new StringTokenizer(reader.readLine(), ",");
            int[] arr = new int[20];

            for (int i = 0; i < 20; i++) {
                arr[i] = Integer.parseInt(st.nextToken());
            }
            for (int i = 0; i < arr.length; i++) {
                for (int j = i + 1; j < 20; j++) {
                    if (arr[j] < arr[i]) {
                        int x = arr[i];
                        arr[i] = arr[j];
                        arr[j] = x;
                    }
                }
            }
            System.out.println("По возрастанию:");
            for (int i : arr) System.out.print(i + " ");
            System.out.println("\n");

            for (int i = 0; i < arr.length; i++) {
                for (int j = i + 1; j < 20; j++) {
                    if (arr[j] > arr[i]) {
                        int x = arr[i];
                        arr[i] = arr[j];
                        arr[j] = x;
                    }
                }
            }
            System.out.println("По убыванию:");
            for (int i : arr) System.out.print(i + " ");
            System.out.println("\n");

        } catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException");
        } catch (IOException e) {
            System.out.println("IOException");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("ArrayIndexOutOfBoundsException");
        }
        //******************************************************************************************
        // Задание 3
        System.out.print("Факториал 20 = ");
        System.out.println(Factorial.factorial(20));

    }
}


