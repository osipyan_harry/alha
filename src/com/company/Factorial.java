package com.company;

public class Factorial {
    public static long factorial(int i) {
        long x = 1;
        for (int j = 1; j <= i; j++) {
            x = j * x;
        }
        return x;
    }
}
